﻿using System.Windows;
using System.Windows.Media;
using RzdTest.Arithmetic.Interfaces;
using RzdTest.Interfaces;

namespace RzdTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(IMainViewModel mainViewModel)
        {
            DataContext = mainViewModel;
            mainViewModel.OnFillingStrategyChanged = FillingStrategyChanged;
            mainViewModel.OnFillingColorChanged = FillingColorChanged;
            InitializeComponent();
        }

        private void FillingStrategyChanged(IFillingStrategy fillingStrategy)
        {
            MainGraphicsField.FillingStrategy = fillingStrategy;
        }

        private void FillingColorChanged(Color color)
        {
            MainGraphicsField.FillingColor = color;
        }
    }
}