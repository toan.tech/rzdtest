﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using RzdTest.Arithemetics.ViewModels;
using RzdTest.Arithmetic.Extensions;
using RzdTest.Arithmetic.Interfaces;

namespace RzdTest.DrawingEngine;

internal sealed class GraphicsField : Canvas
{
    private const int WayThickness = 1;
    private readonly Brush _wayBrush = new SolidColorBrush(Colors.Gray);
    private readonly Brush _pointStroke = new SolidColorBrush(Colors.DarkGray);
    private Color _fillingColor = Colors.Black;
    private IFillingStrategy _fillingStrategy;

    public GraphicsField()
    {
        Fills = new List<Polygon>();
        DrawBasicBackground();
        SeedData();
    }

    public IFillingStrategy FillingStrategy
    {
        get => _fillingStrategy;
        set
        {
            _fillingStrategy = value; 
            FillByStrategy();
        }
    }

    public Color FillingColor
    {
        get => _fillingColor;
        set
        {
            _fillingColor = value;
            FillByStrategy();
        }
    }

    private ICollection<Polygon> Fills { get; }

    private WayPointViewModel InitialPointViewModel { get; set; }

    private void DrawBasicBackground()
    {
        Background = new SolidColorBrush(Colors.White);
    }

    private void RemoveFills()
    {
        foreach (var filling in Fills)
        {
            Children.Remove(filling);
        }
        
        Fills.Clear();
    }

    private void FillByStrategy()
    {
        if (FillingStrategy == null)
            return;
        
        RemoveFills();
        var polygonPoints = FillingStrategy.GetAreaForFilling(InitialPointViewModel)
            .Select(x => new Point(x.X, x.Y))
            .ToList();
        
        if (polygonPoints.Count == 0)
            return;

        var polygon = new Polygon
        {
            StrokeThickness = 1,
            Stroke = new SolidColorBrush(Colors.Black),
            Points = new PointCollection(polygonPoints),
            Fill = new SolidColorBrush(FillingColor),
        };

        Children.Add(polygon);
        Fills.Add(polygon);
        
        DrawText("Парк", polygon.Points.First());
    }

    private void DrawText(string text, Point point)
    {
        const int horizontalOffset = 20;
        var textBlock = new TextBlock
        {
            Text = text,
            Foreground = Brushes.Black,
            FontWeight = FontWeights.Bold,
            FontSize = 14
        };

        Children.Add(textBlock);
        
        SetLeft(textBlock, point.X + horizontalOffset);
        SetTop(textBlock, point.Y);
    }

    private WayPointViewModel AddNewWayPoint(int x, int y, WayPointViewModel parent)
    {
        WayPointViewModel newPointViewModel;
        if (parent == null)
        {
            newPointViewModel = new WayPointViewModel(x, y, new List<WayPointViewModel>());
            InitialPointViewModel = newPointViewModel;
        }
        else
        {
            newPointViewModel = new WayPointViewModel(x, y, new List<WayPointViewModel>());
            if (newPointViewModel.HasCollisions(InitialPointViewModel))
            {
                return null;
            }

            parent.Children.Add(newPointViewModel);
            DrawWay(parent, newPointViewModel);
        }

        DrawWayPoint(newPointViewModel);
        return newPointViewModel;
    }

    private void DrawWay(WayPointViewModel from, WayPointViewModel to)
    {
        var line = new Line()
        {
            StrokeThickness = WayThickness,
            Stroke = _wayBrush,
            X1 = from.X,
            Y1 = from.Y,
            X2 = to.X,
            Y2 = to.Y
        };

        Children.Add(line);
        if (from != to &&
            from.Children.All(x => x != to))
        {
            from.Children.Add(to);
        }
    }

    private void DrawWayPoint(WayPointViewModel wayPointViewModel)
    {
        var circle = new Ellipse()
        {
            Width = WayPointViewModel.PointSize,
            Height = WayPointViewModel.PointSize,
            StrokeThickness = WayPointViewModel.PointSize,
            Stroke = _pointStroke
        };
        
        Children.Add(circle);
        SetLeft(circle, wayPointViewModel.X);
        SetTop(circle, wayPointViewModel.Y);
    }

    private void SeedData()
    {
        // seed points
        var initPoint = AddNewWayPoint(0, 30, null);
        var sidePoint = AddNewWayPoint(20, 10, initPoint);
        WayPointViewModel finishPointViewModel = null;
            
        for (int i = 0; i < 7; i++)
        {
            var point = AddNewWayPoint(40 + i * 20, 10 + i * 20, sidePoint);
            var child = AddNewWayPoint(120 + i * 20, 10 + i * 20, point);
            if (finishPointViewModel == null)
            {
                finishPointViewModel = AddNewWayPoint(400, 50, child);
            }
            else
            {
                child.Children.Add(finishPointViewModel);
                DrawWay(child, finishPointViewModel);
            }
            
            sidePoint = point;
        }
    }
}