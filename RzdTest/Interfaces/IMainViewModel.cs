﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using RzdTest.Arithmetic.Interfaces;
using RzdTest.Models;

namespace RzdTest.Interfaces;

public interface IMainViewModel
{
    ICollection<ColorDetails> FillingColors { get; }
    
    ICollection<FillingStrategyDetails> FillingStrategies { get; }
    
    Action<IFillingStrategy> OnFillingStrategyChanged { get; set; }
    
    Action<Color> OnFillingColorChanged { get; set; }
}