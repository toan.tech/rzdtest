﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using RzdTest.Arithmetic.Interfaces;
using RzdTest.Arithmetic.Strategies;
using RzdTest.Interfaces;
using RzdTest.Models;

namespace RzdTest.ViewModels;

internal sealed class MainViewModel : BaseViewModel, IMainViewModel
{
    private ICollection<ColorDetails> _fillingColors;
    private ICollection<FillingStrategyDetails> _fillingStrategies;
    private FillingStrategyDetails _selectedFillingStrategyDetails;
    private ColorDetails _selectedColor;

    public MainViewModel()
    {
        LoadData();
    }

    public Action<IFillingStrategy> OnFillingStrategyChanged { get; set; }
    
    public Action<Color> OnFillingColorChanged { get; set; }

    public ICollection<ColorDetails> FillingColors
    {
        get => _fillingColors;
        private set => SetProperty(ref _fillingColors, value);
    }

    public ICollection<FillingStrategyDetails> FillingStrategies
    {
        get => _fillingStrategies;
        private set => SetProperty(ref _fillingStrategies, value);
    }

    public ColorDetails SelectedColor
    {
        get => _selectedColor;
        set
        {
            SetProperty(ref _selectedColor, value);
            OnFillingColorChanged(value.Color);
        }
    }

    public FillingStrategyDetails SelectedFillingStrategyDetails
    {
        get => _selectedFillingStrategyDetails;
        set
        {
            SetProperty(ref _selectedFillingStrategyDetails, value);
            OnFillingStrategyChanged?.Invoke(
                (IFillingStrategy)Activator.CreateInstance(_selectedFillingStrategyDetails.FillingStrategyType));
        }
    }

    private void LoadData()
    {
        // some example data
        FillingColors = new[]
        {
            new ColorDetails
            {
                Color = Colors.Red,
                Name = "Красный"
            },
            new ColorDetails
            {
                Color = Colors.Green,
                Name = "Зеленый"
            },
            new ColorDetails
            {
                Color = Colors.Yellow,
                Name = "Желтый"
            },
        };
        
        FillingStrategies = new[]
        {
            new FillingStrategyDetails
            {
                StrategyName = "Полигон",
                FillingStrategyType = typeof(PolygonFillingStrategy)
            }
        };
    }
}