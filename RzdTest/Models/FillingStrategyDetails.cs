﻿using System;

namespace RzdTest.Models;

public class FillingStrategyDetails
{
    public string StrategyName { get; set; }

    public Type FillingStrategyType { get; set; }
}