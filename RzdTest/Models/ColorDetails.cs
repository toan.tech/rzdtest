﻿using System.Windows.Media;

namespace RzdTest.Models;

public sealed class ColorDetails
{
    public Color Color { get; set; }

    public string Name { get; set; }
}