using RzdTest.Arithemetics.ViewModels;
using RzdTest.Arithmetic.Interfaces;
using RzdTest.Arithmetic.Strategies;

namespace RzdTest.Tests;

public class PolygonFillingStrategyTests
{
    private IFillingStrategy _fillingStrategy;
    
    [SetUp]
    public void Setup()
    {
        _fillingStrategy = new PolygonFillingStrategy();
    }

    [Test]
    public void GetAreaForFilling_SinglePoint_EmptyResult()
    {
        var result = 
            _fillingStrategy.GetAreaForFilling(new WayPointViewModel(0,0, new List<WayPointViewModel>()));
        
        Assert.IsEmpty(result);
    }
    
    [Test]
    public void GetAreaForFilling_NegativeCoordinates_EmptyResult()
    {
        var result = 
            _fillingStrategy.GetAreaForFilling(new WayPointViewModel(-10,-20, new List<WayPointViewModel>()));
        
        Assert.IsEmpty(result);
    }
}