﻿using RzdTest.Arithemetics.ViewModels;

namespace RzdTest.Arithmetic.Extensions;

public static class WayPointExtensions
{
    public static bool HasCollisions(this WayPointViewModel wayPointViewModel, WayPointViewModel topLevel)
    {
        if (wayPointViewModel == topLevel)
        {
            return false;
        }

        if (Math.Max(wayPointViewModel.X, topLevel.X) - Math.Min(wayPointViewModel.X, topLevel.X) <= WayPointViewModel.PointSize &&
            Math.Max(wayPointViewModel.Y, topLevel.Y) - Math.Min(wayPointViewModel.Y, topLevel.Y) <= WayPointViewModel.PointSize)
        {
            return true;
        }

        foreach (var item in topLevel.Children)
        {
            if (item == wayPointViewModel)
                continue;

            var hasCollisions = HasCollisions(wayPointViewModel, item);
            if (hasCollisions)
            {
                return true;
            }
        }

        return false;
    }
    
    internal static IEnumerable<WayPointViewModel> GetChildren(this WayPointViewModel pointViewModel)
    {
        foreach (var child in pointViewModel.Children)
        {
            yield return child;
            foreach (var childOfChild in GetChildren(child))
            {
                yield return childOfChild;
            }
        }
    }
}