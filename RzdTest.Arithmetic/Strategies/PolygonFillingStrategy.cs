﻿using RzdTest.Arithemetics.ViewModels;
using RzdTest.Arithmetic.Extensions;
using RzdTest.Arithmetic.Interfaces;

namespace RzdTest.Arithmetic.Strategies;

public sealed class PolygonFillingStrategy : IFillingStrategy
{
    public IEnumerable<WayPointViewModel> GetAreaForFilling(WayPointViewModel startPointViewModel)
    {
        // this is a sample algorithm

        startPointViewModel = FindMultiPoint(startPointViewModel);
        if (startPointViewModel == null)
        {
            yield break;
        }
        
        var ways = startPointViewModel.Children
            .ToDictionary<WayPointViewModel, WayPointViewModel, ICollection<WayPointViewModel>>(child => child,
                child => child.GetChildren().ToList());

        WayPointViewModel commonWayPointViewModel = null;

        foreach (var outerWay in ways)
        {
            foreach (var innerWay in ways)
            {
                if (outerWay.Key == innerWay.Key)
                    continue;

                var result = outerWay.Value.Intersect(innerWay.Value).ToList();
                if (result.Count == 0)
                    continue;

                commonWayPointViewModel = result.First();
                yield return startPointViewModel;
                yield return innerWay.Key;
                foreach (var innerWayItem in innerWay.Value.TakeWhile(x => x != commonWayPointViewModel))
                {
                    yield return innerWayItem;
                }

                yield return commonWayPointViewModel;
            }

            if (commonWayPointViewModel == null)
                continue;

            var filteredOuterWay = outerWay.Value.TakeWhile(x => x != commonWayPointViewModel).ToArray();
            Array.Reverse(filteredOuterWay);
            foreach (var outerItem in filteredOuterWay)
            {
                yield return outerItem;
            }

            yield return outerWay.Key;
            yield return startPointViewModel;
            break;
        }
    }

    private WayPointViewModel FindMultiPoint(WayPointViewModel startPointViewModel)
    {
        if (startPointViewModel.Children.Count > 1)
        {
            return startPointViewModel;
        }

        foreach (var item in startPointViewModel.Children)
        {
            var searchResult = FindMultiPoint(item);
            if (searchResult != null)
            {
                return searchResult;
            }
        }

        return null;
    }
}