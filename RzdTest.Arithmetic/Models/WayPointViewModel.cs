﻿namespace RzdTest.Arithemetics.ViewModels;

public class WayPointViewModel
{
    public static readonly int PointSize = 5;
    
    public WayPointViewModel(int x, int y, IList<WayPointViewModel> children)
    {
        X = x;
        Y = y;
        Children = children;
    }

    public int X { get; set; }

    public int Y { get; set; }

    public ICollection<WayPointViewModel> Children { get; set; }
}