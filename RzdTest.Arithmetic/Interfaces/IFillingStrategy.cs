﻿using RzdTest.Arithemetics.ViewModels;

namespace RzdTest.Arithmetic.Interfaces;

public interface IFillingStrategy
{
    IEnumerable<WayPointViewModel> GetAreaForFilling(WayPointViewModel startPointViewModel);
}